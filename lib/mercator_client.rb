require "rest-client"
require "uri"
require "json"

require "mercator_client/version"
require "mercator_client/domain"
require "mercator_client/factories"
require "mercator_client/api"
require "mercator_client/client"

module MercatorClient
  class Configuration
    attr_accessor :host_url
  end

  @configuration = Configuration.new

  def self.configure(&block)
    yield(@configuration)
  end

  def self.configuration
    @configuration
  end

  def self.client
    @client ||= MercatorClient::Client.new configuration
  end
end

