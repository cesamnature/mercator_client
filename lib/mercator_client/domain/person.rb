class MercatorClient::Domain::Person
  attr_accessor :id, :location

  def mercator_save
    if self.id == nil
      saved   = MercatorClient.client.save_person(self)
      self.id = saved.id
    else
      saved   = MercatorClient.client.update_person(self)
    end
    saved
  end

  def as_json
    json = super
    json.each do | key, value |
      json[key] = value.as_json
    end
    json
  end
end
