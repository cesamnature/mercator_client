class MercatorClient::Domain::Location
  attr_accessor :id, :address, :latitude, :longitude, :formatted_address,
                :administrative_district, :administrative_sub_district,  :country, :postal_code, :locality
end
