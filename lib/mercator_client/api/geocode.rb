module MercatorClient::API
  module Geocode

    def reverse_geocode(address, user_location_latitude, user_location_longitude)
      user_location   = { latitude: user_location_latitude, longitude: user_location_longitude }
      json_locations  = get("reverse_geocoding", { query: address, userLocation: user_location.to_json })
      json_locations.inject([]) do | locations, json_location |
        locations.push(MercatorClient::Factories::Location.create_from_json(json_location))
      end
    end

  end
end
