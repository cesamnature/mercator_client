module MercatorClient::API
  module Person
    def save_person(person)
      person_json = post("people", person.as_json)
      MercatorClient::Factories::Person.create_from_json(person_json)
    end

    def update_person(person)
      person_json         = person.as_json
      updated_person_json = put("people/#{person.id}", { "person" => person_json })
      MercatorClient::Factories::Person.create_from_json(updated_person_json)
    end

    def people_around(center, range)
      people_json = get("people_around", { center_longitude: center["longitude"], center_latitude: center["latitude"], range: range })
      people_json.map do | person_json |
        MercatorClient::Factories::Person.create_from_json(person_json)
      end
    end
  end
end
