class MercatorClient::Client
  attr_accessor :host_url

  include MercatorClient::API

  def initialize(configuration)
    @host_url    = configuration.host_url
  end

  def get(path, params = {}, json = true)
    safe_request json do
      RestClient.get compute_path(path), { params: params }
    end
  end

  def post(path, params = {}, json = true)
    safe_request json do
      RestClient.post compute_path(path), params.to_json, content_type: :json, accept: :json
    end
  end

  def delete(path, params = {}, json = true)
    safe_request json do
      RestClient.delete compute_path(path), { params: params }
    end
  end

  def put(path, params = {}, json = true)
    safe_request json do
      RestClient.put compute_path(path), params
    end
  end

  private

  def compute_path(path)
    URI.join(@host_url, path).to_s
  end

  def safe_request(json, &block)
    begin
      response = yield
      if response && response.strip.length > 0 && json
        JSON.parse response
      else
        response
      end
    rescue Errno::ECONNREFUSED => e
      puts e.inspect
      raise MercatorClient::API::ServerError.new(e)
    rescue RestClient::InternalServerError => e
      puts e.inspect
      raise MercatorClient::API::ServerError.new(e)
    end
  end

end
