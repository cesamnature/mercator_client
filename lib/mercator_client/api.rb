require "mercator_client/api/geocode"
require "mercator_client/api/server_error"
require "mercator_client/api/person"

module MercatorClient
  module API
    include MercatorClient::API::Geocode
    include MercatorClient::API::Person
  end
end
