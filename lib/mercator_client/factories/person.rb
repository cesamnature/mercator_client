class MercatorClient::Factories::Person
  def self.create_from_json(json)
    person          = MercatorClient::Domain::Person.new
    person.id       = json["id"]
    person.location = MercatorClient::Factories::Location.create_from_json(json["location"]) if json["location"]
    person
  end
end
