class MercatorClient::Factories::Location
  def self.create_from_json(json)
    location                              = MercatorClient::Domain::Location.new
    location.id                           = json["id"]
    location.address                      = json["address"]
    location.latitude                     = json["latitude"]
    location.longitude                    = json["longitude"]
    location.formatted_address            = json["formattedAddress"]
    location.administrative_district      = json["administrativeDistrict"]
    location.administrative_sub_district  = json["administrativeSubDistrict"]
    location.country                      = json["country"]
    location.postal_code                  = json["postalCode"]
    location.locality                     = json["locality"]
    location
  end
end
